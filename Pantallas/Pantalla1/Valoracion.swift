//
//  Resumen.swift
//  Pantallas
//
//  Created by Carlos Vera Baca on 4/06/21.
//

import SwiftUI

struct Valoracion: View {
    var body: some View {
        VStack(alignment: .trailing, spacing:0) {
            Image("fordferrari")
                .resizable()
                .scaledToFit()
                //.frame(width: .infinity, height: 250)
                .cornerRadius(40)
            ZStack(alignment: .bottomTrailing){
                Capsule()
                    .fill(Color.white)
                    .frame(height: 80)
                    .shadow(radius: 20, x: 0, y: 10)
                Rectangle()
                    .fill(Color.white)
                    .frame(width: 300, height: 80)
                    .overlay(
                        HStack(alignment: .top, spacing: 50){
                            Valoraciones(imagen: "star.fill")
                            RateThis(imagen: "star")
                            MetaScore(imagen: "square.fill")
                        }
                    //.offset(x: 20 , y: 0)
                    )
            }.offset(x: 0, y: -35)
            .padding(.leading,30)
        }
    }
}

struct Resumen_Previews: PreviewProvider {
    static var previews: some View {
        Valoracion()
    }
}

struct Valoraciones: View {
    var imagen: String
    var body: some View {
        VStack{
            Image(systemName: imagen)
                .frame(width: 30, height: 30)
                .font(.system(size: 25))
                .foregroundColor(.green)
            HStack(spacing: 0) {
                Text("8.2")
                    .bold()
                    .font(.system(size: 13, design: .rounded))
                Text("/10")
                    .font(.system(size: 11, design: .rounded))
            }
            Text("150.212")
                .foregroundColor(.secondary)
                .font(.system(size: 10, design: .rounded))

        }
    }
}

struct RateThis: View {
    var imagen: String
    var body: some View {
        VStack(spacing: 5){
            Image(systemName: imagen)
                .frame(width: 30, height: 30)
                .font(.system(size: 25))
            Text("Rate this")
                .bold()
                .font(.system(size: 13, design: .rounded))

        }
    }
}

struct MetaScore: View {
    var imagen: String
    var body: some View {
        VStack(spacing: 5){
            Image(systemName: imagen)
                .frame(width: 30, height: 30)
                .font(.system(size: 25))
                .foregroundColor(.green)
                .overlay(
                    Text("86")
                        .foregroundColor(.white)
                        .font(.system(size: 11, design: .rounded))
                )
            Text("Metascore")
                .bold()
                .font(.system(size: 13, design: .rounded))

        }
    }
}


