//
//  ContentView.swift
//  Pantallas
//
//  Created by Carlos Vera Baca on 30/05/21.
//

import SwiftUI

struct ContentViewPant1: View {
    var body: some View {
        ZStack {
            Color(UIColor.cyan)
            ZStack{
                Color(.white)
                    .cornerRadius(40)
                VStack(alignment: .leading, spacing: 0){
                    Valoracion()
                    Comparacion()
                        .padding(.horizontal,30)
                    PlotSummary()
                        .padding(.horizontal, 30)
                        .padding(.top, 25)
                    CastCrew()
                        .padding(.horizontal, 30)
                        .padding(.top, 25)
                    Spacer()
                }
            }.padding(10)
        }
    }
}

struct ContentViewPant1_Previews: PreviewProvider {
    static var previews: some View {
        ContentViewPant1()
    }
}
