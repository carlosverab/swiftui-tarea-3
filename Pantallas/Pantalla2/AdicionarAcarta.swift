//
//  AdicionarAcarta.swift
//  Pantallas
//
//  Created by Carlos Vera Baca on 6/06/21.
//

import SwiftUI

struct AdicionarAcarta: View {
    var body: some View {
        Button(action: {

        }, label: {
            Text("Add to cart")
                .bold()
            .padding(20)
            .frame(maxWidth: .infinity)
        })
        .foregroundColor(.white)
        .background(Color.blue)
        .clipShape(Capsule())
    }
}

struct AdicionarAcarta_Previews: PreviewProvider {
    static var previews: some View {
        AdicionarAcarta()
    }
}
