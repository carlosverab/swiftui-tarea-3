//
//  Comparacion.swift
//  Pantallas
//
//  Created by Carlos Vera Baca on 4/06/21.
//

import SwiftUI

struct Comparacion: View {
    var body: some View {
        ZStack(alignment: .topTrailing){
            VStack{
                HStack(alignment: .top) {
                    Text("Ford v Ferrari")
                        .bold()
                        .font(.system(size: 28, design: .rounded))
                    Spacer()
                   
                }
                
                HStack(spacing:18){
                    Text("2019")
                    Text("PG-13")
                    Text("2h 32min")
                    Spacer()
                }
                .font(.system(size: 15, design: .rounded))
                .foregroundColor(.secondary)
                .padding(.vertical,5)
                
                HStack(spacing:18){
                    AccionesTexto(accion: "Action")
                    AccionesTexto(accion: "Biography")
                    AccionesTexto(accion: "Drama")
                    Spacer()
                }
            }
            Button(action: {
                
            }, label: {
                Image(systemName: "plus.app.fill")
                    .frame(width: 55, height: 55)
                    .font(.system(size: 60))
                    .foregroundColor(.gray)
                    .background(Color.white)
            })
        }
    }
}

struct Comparacion_Previews: PreviewProvider {
    static var previews: some View {
        Comparacion()
    }
}

struct AccionesTexto: View {
    var accion: String
    var body: some View {
        Text(accion)
            .padding(10)
            .overlay(
                RoundedRectangle(cornerRadius: 20)
                    .stroke(Color(UIColor.lightGray), lineWidth: 2)
            )
    }
}
