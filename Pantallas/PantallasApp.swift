//
//  PantallasApp.swift
//  Pantallas
//
//  Created by Carlos Vera Baca on 30/05/21.
//

import SwiftUI

@main
struct PantallasApp: App {
    var body: some Scene {
        WindowGroup {
            ContentViewPant1()
        }
    }
}
