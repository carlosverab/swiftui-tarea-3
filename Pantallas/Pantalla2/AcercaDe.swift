//
//  AcercaDe.swift
//  Pantallas
//
//  Created by Carlos Vera Baca on 6/06/21.
//

import SwiftUI

struct AcercaDe: View {
    var body: some View {
        VStack(alignment: .leading) {
            Text("About")
                .font(.system(size: 23, design: .rounded))
                .bold()
            Text("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla porttitor nunc id augue volutpat posuere. Proin sodales ac neque a tristique. Maecenas euismod nulla et efficitur.")
                .font(.system(size: 14, design: .rounded))
                .foregroundColor(.gray)
        }
    }
}

struct AcercaDe_Previews: PreviewProvider {
    static var previews: some View {
        AcercaDe()
    }
}
