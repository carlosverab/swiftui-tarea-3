//
//  Plato.swift
//  Pantallas
//
//  Created by Carlos Vera Baca on 6/06/21.
//

import SwiftUI

struct Plato: View {
    var body: some View {
        HStack(spacing:2){
            Text("Maki Mix")
                .font(.largeTitle)
                .bold()
            Spacer()
            Text("$")
                .font(.system(size: 16))
                .foregroundColor(.green)
                .bold()
            Text("16.00")
                .font(.title)
                .foregroundColor(.green)
                .bold()
        }
    }
}


struct Plato_Previews: PreviewProvider {
    static var previews: some View {
        Plato()
    }
}
