//
//  Cast&Crew.swift
//  Pantallas
//
//  Created by Carlos Vera Baca on 5/06/21.
//

import SwiftUI

struct CastCrew: View {
    var body: some View {
        VStack(alignment: .leading){
            Text("Cast & Crew")
                .font(.system(size: 23, design: .rounded))
            HStack(spacing: 25){
                Personaje(imagen: "caitriona", nombre: "Caitriona", apellido: "Balfe", personifica: "Mollie")
                Personaje(imagen: "mattdamon", nombre: "Matt", apellido: "Damon", personifica: "Caroll")
                Personaje(imagen: "cristianbale", nombre: "Christian", apellido: "Balfe", personifica: "Ken Miles")
                Personaje(imagen: "jamesmangold", nombre: "James", apellido: "Mangold", personifica: "Director")
            }
        }
    }
}

struct CastCrew_Previews: PreviewProvider {
    static var previews: some View {
        CastCrew()
    }
}

struct Personaje: View {
    var imagen: String
    var nombre: String
    var apellido: String
    var personifica: String
    
    var body: some View {
        VStack{
            Image(imagen)
                .resizable()
                .scaledToFill()
                .frame(width: 60, height: 60)
                .clipShape(Circle())
            Text(nombre)
                .font(.system(size: 11, design: .rounded))
                .bold()
            Text(apellido)
                .font(.system(size: 11, design: .rounded))
                .bold()
            Text(personifica)
                .font(.system(size: 11, design: .rounded))
                .foregroundColor(.secondary)
            
        }
    }
}
