//
//  PlotSummary.swift
//  Pantallas
//
//  Created by Carlos Vera Baca on 5/06/21.
//

import SwiftUI

struct PlotSummary: View {
    var body: some View {
        VStack(alignment: .leading, spacing: 10){
            Text("Plot Summary")
                .font(.system(size: 23, design: .rounded))
            Text("Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's.")
                .font(.system(size: 14, design: .rounded))
                .foregroundColor(.secondary)

        }
    }
}

struct PlotSummary_Previews: PreviewProvider {
    static var previews: some View {
        PlotSummary()
    }
}
