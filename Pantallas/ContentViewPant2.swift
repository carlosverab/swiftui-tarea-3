//
//  ContentViewPant2.swift
//  Pantallas
//
//  Created by Carlos Vera Baca on 6/06/21.
//

import SwiftUI

struct ContentViewPant2: View {
    var body: some View {
        ZStack(alignment: .top) {
            Color(UIColor.cyan)
            ZStack(alignment: .bottom){
                Color(.magenta)
                    .cornerRadius(40)
                Color(.white)
                    .frame(height: 500)
                    .cornerRadius(40)
                DetailView()
            }.padding(10)
            Image("sushi-5")
                .resizable()
                .scaledToFit()
                .frame(width: 200, height: 200)
                .clipShape(/*@START_MENU_TOKEN@*/Circle()/*@END_MENU_TOKEN@*/.inset(by: 10))
                .offset(x: 0, y: 155)
            Image(systemName: "heart")
                .foregroundColor(.white)
                .offset(x: 130, y: 50)
                .font(.largeTitle)
        }
    }
}

struct DetailView: View {
    var body: some View {
        VStack {
            Plato()
            ValoracionPlato()
            AcercaDe()
                .padding(.vertical,40)
            AdicionarAcarta()
        }.padding(30)
        
    }
}

struct ContentViewPant2_Previews: PreviewProvider {
    static var previews: some View {
        ContentViewPant2()
    }
}
