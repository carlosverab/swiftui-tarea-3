//
//  ValoracionDePlato.swift
//  Pantallas
//
//  Created by Carlos Vera Baca on 6/06/21.
//

import SwiftUI

struct ValoracionPlato: View {
    var body: some View {
        HStack{
            Capsule()
                .frame(width: 80, height: 30)
                .foregroundColor(.blue)
                .overlay(
                    Label("30m", systemImage: "timer")
                        .foregroundColor(.white)
                )
            Capsule()
                .frame(width: 80, height: 30)
                .foregroundColor(.blue)
                .overlay(
                    Label{
                        Text("4.5")
                            .font(.subheadline)
                    } icon: {
                        Image(systemName: "star")
                    }.foregroundColor(.white)
                )
            Spacer()
        }
    }
}

struct ValoracionPlato_Previews: PreviewProvider {
    static var previews: some View {
        ValoracionPlato()
    }
}
